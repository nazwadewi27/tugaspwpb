<?php

namespace App\Http\Controllers;


use App\Models\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d =  Siswa::get();

        return view('siswa.index' , compact('d'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'nis'=>'required',
            'nama'=>'required',
            'kelas'=>'required',
            'jurusan'=>'required',

        ]);

        $d = new Siswa();
        $d->nis = $request->nis;
        $d->nama = $request->nama;
        $d->kelas = $request->kelas;
        $d->jurusan = $request->jurusan;

        $d->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $d = Siswa::where('id' , $id)->firstOrFail();

        $this->validate($request , [
            'nis'=>'required',
            'nama'=>'required',
            'kelas'=>'required',
            'jurusan'=>'required',

        ]);

        $d->nis = $request->nis;
        $d->nama = $request->nama;
        $d->kelas = $request->kelas;
        $d->jurusan = $request->jurusan;

        $d->update();

        return redirect()->back();
    }



    public function search (Request $request) {
        $d = Siswa::where('nis' , 'Like' , '%' . $request->search . '%')
                        ->orWhere('nama' , 'Like' , '%' . $request->search . '%')
                        ->orWhere('kelas' , 'Like' , '%' . $request->search . '%')
                        ->orWhere('jurusan' , 'Like' , '%' . $request->search . '%')
                        ->get();

                        return view('siswa.index' , compact('d'));
    }
    
    //sort nama
    public function sortASCnama(Request $request) {
        $d = Siswa::orderBy('nama' , 'asc')->get();

        return view('siswa.index' , compact('d'));
    }

    public function sortDESCnama(Request $request) {
        $d = Siswa::orderBy('nama' , 'desc')->get();

        return view('siswa.index' , compact('d'));
    }

    //sort nis
    public function sortASCnis(Request $request) {
        $d = Siswa::orderBy('nis' , 'asc')->get();

        return view('siswa.index' , compact('d'));
    }

    public function sortDESCnis(Request $request) {
        $d = Siswa::orderBy('nis' , 'desc')->get();

        return view('siswa.index' , compact('d'));
    }

    //sort kelas
    public function sortASCkelas(Request $request) {
        $d = Siswa::orderBy('kelas' , 'asc')->get();

        return view('siswa.index' , compact('d'));
    }

    public function sortDESCkelas(Request $request) {
        $d = Siswa::orderBy('kelas' , 'desc')->get();

        return view('siswa.index' , compact('d'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $d = Siswa::find($id);
        $d->delete();

        return redirect()->back();
    }
}
