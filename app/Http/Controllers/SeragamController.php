<?php

namespace App\Http\Controllers;

use App\Models\Seragam; 
use Illuminate\Http\Request;

class SeragamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d = Seragam::get();

        return view('seragam.index' , compact('d'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request  ,[
            'nama'=>'required',
            'harga'=>'required',
            'ukuran'=>'required'



        ]);

        $d = new Seragam();
        $d->nama = $request->nama;
        $d->harga = $request->harga;
        $d->ukuran = $request->ukuran;


        $d->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $d = Seragam::where('id' , $id)->firstOrFail();

        $this->validate($request  ,[
            'ukuran'=>'required',
            'nama'=>'required',
            'harga'=>'required'


        ]);

        $d->ukuran = $request->ukuran;
        $d->nama = $request->nama;
        $d->harga = $request->harga;

        $d->update();

        return redirect()->back();
    }




    public function search (Request $request) {
        $d = Seragam::where('ukuran' , 'Like' , '%' . $request->search . '%')
                        ->orWhere('nama' , 'Like' , '%' . $request->search . '%')
                        ->orWhere('harga' , 'Like' , '%' . $request->search . '%')
                        ->get();

                        return view('seragam.index' , compact('d'));
    }   


    public function sortASCnama(Request $request) {
        $d = Seragam::orderBy('nama' , 'asc')->get();

        return view('seragam.index' , compact('d'));
    }

    public function sortDESCnama(Request $request) {
        $d = Seragam::orderBy('nama' , 'desc')->get();

        return view('seragam.index' , compact('d'));
    }

    //sort harga
    public function sortASCharga(Request $request) {
        $d = Seragam::orderBy('harga' , 'asc')->get();

        return view('seragam.index' , compact('d'));
    }

    public function sortDESCharga(Request $request) {
        $d = Seragam::orderBy('harga' , 'desc')->get();

        return view('seragam.index' , compact('d'));
    }

    //sort ukuran
    public function sortASCukuran(Request $request) {
        $d = Seragam::orderBy('ukuran' , 'asc')->get();

        return view('seragam.index' , compact('d'));
    }

    public function sortDESCukuran(Request $request) {
        $d = Seragam::orderBy('ukuran' , 'desc')->get();

        return view('seragam.index' , compact('d'));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $d = Seragam::find($id);
        $d->delete();

        return redirect()->back();
    }


    public function filterUkuranS (Request $request) {
        if($request->uk_s ==null){
            $request->uk_s = 's';
            $req = $request->uk_s;
            $d = Seragam::where('ukuran' , 'like' , $req)->get();
        }

        return view('seragam.index' , compact('d'));
    }

    public function filterUkuranM (Request $request) {
        if($request->uk_m ==null){
            $request->uk_m = 'm';
            $req = $request->uk_m;
            $d = Seragam::where('ukuran' , 'like' , $req)->get();
        }

        return view('seragam.index' , compact('d'));
    }

    public function filterUkuranL (Request $request) {
        if($request->uk_l ==null){
            $request->uk_l = 'l';
            $req = $request->uk_l;
            $d = Seragam::where('ukuran' , 'like' , $req)->get();
        }

        return view('seragam.index' , compact('d'));
    }

    public function filterUkuranXL (Request $request) {
        if($request->uk_xl ==null){
            $request->uk_xl = 'xl';
            $req = $request->uk_xl;
            $d = Seragam::where('ukuran' , 'like' , $req)->get();
        }

        return view('seragam.index' , compact('d'));
    }
}
